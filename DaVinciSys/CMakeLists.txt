################################################################################
# Package: DaVinciSys
################################################################################
gaudi_subdir(DaVinciSys)

gaudi_depends_on_subdirs(DaVinciTests
                         ##############################
                         # Temporarily moved in from Stripping
                         Phys/StrippingCache
                         ##############################
                         Phys/DaVinci
                         Phys/KaliCalo
                         Phys/Tesla
                         Phys/TurboCache
                         )

gaudi_add_test(QMTest QMTEST)
