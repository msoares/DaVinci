"""Invalid configuration for running over Turbo data."""
from Configurables import DaVinci

dv = DaVinci()
dv.DataType = '2016'
dv.InputType = 'MDST'
dv.Turbo = True
# The absence of this setting should cause DaVinci to throw an exception
# dv.RootInTES = '/Event/Turbo'
