from Configurables import DaVinci
DaVinci().TupleFile = 'DV-Ntuple.root'
DaVinci().HistogramFile = 'DV-Histos.root'
DaVinci().DataType = 'Upgrade'
DaVinci().Lumi = False
DaVinci().Simulation = True
