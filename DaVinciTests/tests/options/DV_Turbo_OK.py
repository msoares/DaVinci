"""Valid configuration for running over Turbo data."""
from Configurables import DaVinci

dv = DaVinci()
dv.DataType = '2016'
dv.InputType = 'MDST'
dv.Turbo = True
dv.RootInTES = '/Event/Turbo'
