"""Valid configuration for running over Turbo data.

We don't set InputType to MDST explicitly, but this should be done by the
DaVinci configuration.
"""
from Configurables import DaVinci

dv = DaVinci()
dv.DataType = '2016'
# This value should be set automatically as Turbo = True (although it's better
# to set it explicitly anyway)
# dv.InputType = 'MDST'
dv.Turbo = True
dv.RootInTES = '/Event/Turbo'
