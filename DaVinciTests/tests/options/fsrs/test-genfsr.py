from Gaudi.Configuration import *

from Configurables import GaudiSequencer
seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += [ "GenFSRMerge" ]
seqGenFSR.Members += [ "GenFSRLog" ]

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20130929-1"
LHCbApp().CondDBtag = "sim-20130522-1-vc-md100"

from Configurables import DaVinci
DaVinci().InputType = "DST"
DaVinci().DataType  = "2012"
DaVinci().Simulation   = True
DaVinci().UserAlgorithms = [seqGenFSR]

from PRConfig import TestFileDB
TestFileDB.test_file_db["genFSR_2012_dst"].run(configurable=DaVinci())
