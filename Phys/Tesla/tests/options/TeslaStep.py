from Configurables import Tesla
Tesla().Pack = True
Tesla().InputType = "RAW"
Tesla().DataType = '2016'
Tesla().Simulation = False
Tesla().Mode = 'Online'
Tesla().VertRepLoc = 'Hlt2'
Tesla().KillInputHlt2Reps = True
Tesla().SplitRawEventInput = '0.4'
Tesla().SplitRawEventOutput = '0.4'

from Configurables import Tesla
from Gaudi.Configuration import *

from PRConfig import TestFileDB
TestFileDB.test_file_db['TeslaTest_2016raw_0x11361609_0x21361609'].run(configurable=Tesla())
Tesla().EvtMax = 50
Tesla().OutputLevel = 4

# Timing table to make sure things work as intended
from Configurables import AuditorSvc, LHCbTimingAuditor
ApplicationMgr().AuditAlgorithms = 1
if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
    ApplicationMgr().ExtSvc.append('AuditorSvc')
AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))
# Print algorithm name with 40 characters
from Configurables import MessageSvc
MessageSvc().Format = '% F%40W%S%7W%R%T %0W%M'
