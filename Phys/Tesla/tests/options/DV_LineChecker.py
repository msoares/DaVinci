from Configurables import DaVinci

from Configurables import TeslaLineChecker

from PRConfig import TestFileDB
TestFileDB.test_file_db['TeslaTest_2016raw_0x11361609_0x21361609'].run(configurable=DaVinci())

# Necessary DaVinci parameters #################
DaVinci().Simulation   = False
DaVinci().EvtMax = 50
DaVinci().TupleFile = 'DVTuples.root'
DaVinci().PrintFreq = 10
DaVinci().DataType      = '2016'
DaVinci().UserAlgorithms = [ TeslaLineChecker() ] 
DaVinci().InputType = 'MDST'
