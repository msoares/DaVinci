from GaudiConf import IOHelper
IOHelper().inputFiles(['PFN:DiMuon.dst'])
from Configurables import DaVinci
DaVinci().TupleFile = 'TupleStepStreams.root'
DaVinci().RootInTES = '/Event/DiMuon/Turbo'
