from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import GaudiSequencer,RawEventJuggler
from Configurables import DaVinci
from Configurables import DecodeRawEvent

DaVinci()
DaVinci().EvtMax=-1
DaVinci().Lumi=True
DaVinci().DataType="2016"
DaVinci().Turbo=True

from GaudiConf import IOHelper
IOHelper().inputFiles( ["DiMuon.dst"] , clear=True ) 
