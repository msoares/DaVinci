from GaudiConf import IOHelper
IOHelper().inputFiles(['PFN:Tesla.dst'])
from Configurables import DaVinci
DaVinci().TupleFile = 'TupleStepOneLine.root'
DaVinci().RootInTES = '/Event/Turbo'
