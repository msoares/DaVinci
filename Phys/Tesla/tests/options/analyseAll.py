from subprocess import call
import os
execfile(os.path.expandvars("$TESLAROOT/tests/options/streams.py"))
exit_code = call(["python",os.path.expandvars("$TESLAROOT/tests/options/analyseInput.py")])
if exit_code:
    exit( exit_code )

for stream,lines in turbo_streams.iteritems():
    exit_code = call(["python",os.path.expandvars("$TESLAROOT/tests/options/analyseOutput.py"),stream])
    if exit_code:
        exit( exit_code )
