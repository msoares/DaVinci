import os
execfile(os.path.expandvars("$TESLAROOT/tests/options/streams.py"))
from Configurables import Tesla
Tesla().Streams = turbo_streams
Tesla().IgnoredLines = [".*TurboCalib"]
Tesla().EvtMax = 500
import pickle
pickle.dump( turbo_streams, open( "streams.p", "wb" ) )
