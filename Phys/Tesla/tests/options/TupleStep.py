from Configurables import DaVinci
from Configurables import DecayTreeTuple

Tuple = DecayTreeTuple("psi")
Tuple.Decay = "J/psi(1S) -> mu+ mu-"
Tuple.Inputs = [ '/Event/Turbo/Hlt2DiMuonJPsiTurbo/Particles']

# Necessary DaVinci parameters #################
DaVinci().Simulation   = False
DaVinci().SkipEvents = 0
DaVinci().EvtMax = -1
DaVinci().Lumi = True
DaVinci().TupleFile = 'DVTuples.root'
DaVinci().PrintFreq = 10
DaVinci().DataType      = '2016'
DaVinci().UserAlgorithms = [] 
DaVinci().UserAlgorithms += [Tuple]
DaVinci().InputType = 'MDST'
DaVinci().Turbo = True
