import logging
from GaudiKernel.ProcessJobOptions import GetConsoleHandler

# Do not print any of the messages with level lower than WARNING
GetConsoleHandler().disable(allowed=logging.WARNING)
